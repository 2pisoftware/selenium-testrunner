# test-runner and assorted testing utilities

### Running tests:
```php testrunner.php /path/to/cmfive-boilerplate http://cmfiveurl.com [true]```

Setting the third argument to true will invoke selenium in graphical mode rather than a virtual framebuffer.

### Writing tests:

