<?php
/**
* Testrunner
* @author isaac lynnah, isaac@2pisoftware.com, February 2017
*
* To run tests place this file above your cmfive root directory
* and on the command line type ' sudo php testrunner.php your/cmfive/root/path "http://your.url"'
*
*/

//receive the root directory from command parameters
$root_path = $argv[1];
$url = $argv[2];
$test_name = $argv[3];


//First check that a dependant software is installed
//check that java is installed
echo exec("apt-get install default-jdk -y");
echo 'test';
//check that the correct version of firefox is installed. testrunner requires firefox 47
exec("firefox -version", $firefox_version);
echo $firefox_version[0]; echo "\n";
if( $firefox_version[0] == "Mozilla Firefox 47.0.1") {
	echo "Correct firefox version\n";
} else {
	echo "Wrong firefox version or firefox not installed\n";
	echo "removing wrong version\n";
	exec("apt-get purge firefox");
	echo "Installing correct firefox\n";
	exec("wget https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/47.0.1/linux-x86_64/en-US/firefox-47.0.1.tar.bz2");
	exec("tar -xjvf firefox-47.0.1.tar.bz2");
	exec("rm -rf /opt/firefox*");
	exec("mv firefox /opt/firefox");
	exec("ln -sf /opt/firefox/firefox /usr/bin/firefox");
	//check new version
	exec("firefox -version", $firefox_version2);
	if( $firefox_version2[0] == "Mozilla Firefox 47.0.1") {
		echo "correct firefox version installed\n";
	} else {
		echo "unable to install firefox properly - please investigate\n";
		exit();
	}
}
//check that selenium server 2.53.1 is installed
if (file_exists("/usr/bin/selenium-server-standalone-2.53.1.jar")) {
	echo "selenium found\n";
} else  {
	echo "selenium not found\n";
	exec("wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar -P /usr/bin");
	if (file_exists("/usr/bin/selenium-server-standalone-2.53.1.jar")) {
		echo "selenium downloaded\n";
	} else {
		echo "selenium failed to download - please investigate\n";
		exit();
	}
}
//all dependencies now accounted for.

//instantiate cmfive Web for configuration access

//move to the project directory
//-------------------------------------------------
chdir($root_path);
// require Web. needed for accessing database credentials
require "system/web.php";
//instanciate Web
$w = new Web();

//ensure any new log files have the correct permissions
//ensure a log is created
$w->log->setLogger("TESTING")->info("This is the Test Runnern");

exec("chmod -R oug+wrx " . $root_path . "/log");

//user Web functions to create database connection
$w->initDB();
$results = [];

//take a dump
//of the database to restore after testing
$dump_file = "../db_dump.sql";
exec("mysqldump -u" . Config::get('database.username') . " -p" . Config::get('database.password') . " " . Config::get('database.database') . " > " . $dump_file);
if (file_exists($dump_file)) {
	echo "db backup successful\n";
} else {
	echo "db backup failed\n";
}

//create function for emptying the database
function cleanDatabase($w) {
	$tb_results = $w->db->query("SHOW TABLES;");
	if(isset($tb_results)) {
        if($tb_results->rowCount() > 0) {
        	//echo " begin dropping tables\n";
            // drop each table
            foreach($tb_results as $row) {
            	//echo "emptying table " . $row[0] . "\n";
            	$w->db->clear_sql();
                $w->db->exec("TRUNCATE TABLE {$row[0]};");
                //echo "{$row[0]}<br/>";
                $w->db->clear_sql();
                $t_count = $w->db->query("SELECT COUNT(*) FROM {$row[0]};")->fetchAll();
                //print_r($t_count);
                if ($t_count[0][0] > 0) {
                	echo "truncate failed for table: " . $row[0] . ". Currently has " . $t_count[0][0] . "\n";
                }
            }
        }
        // else it's empty... all is well... proceed...
    } else {
        throw new Exception("Could not retrieve tables for \"". $db_name . "\"");
    }
}

//function for restoring the database
function DropTablesDatabase($w) {
	//echo "begin database cleanup\n";
	$db_name = Config::get('database.database');
	// echo "retrieving table names\n";
	$tb_results = $w->db->query("SHOW TABLES;");
    if(isset($tb_results)) {
        if($tb_results->rowCount() > 0) {
        	// echo " begin dropping tables\n";
            // drop each table
            foreach($tb_results as $row) {
            	// echo "dropping table " . $row[0] . "\n";
            	$w->db->clear_sql();
                $w->db->exec("DROP TABLE {$row[0]};");
                //echo "{$row[0]}<br/>";
            }
            // echo "rechecking tables\n";
            $tb_results_second_opinion = $w->db->query("SHOW TABLES;");
            if(isset($tb_results_second_opinion)) {
                if($tb_results_second_opinion->rowCount() > 0) {
                    throw new Exception("Database \"". $db_name . "\" could not be emptied. " . "Currently has " . $tb_results_second_opinion->rowCount() . " rows");
                }
            } else {
                throw new Exception("Could not perform sql recount of tables for \"". $db_name . "\".");
            }
        }
        // else it's empty... all is well... proceed...
    } else {
        throw new Exception("Could not retrieve tables for \"". $db_name . "\"");
    }

    // Run migrations

    // this won't work as it assigns the database to be a PDO object and not a DbPDO object
    // $w->db = $pdo;

    return;
}

function RestoreDatabase($w, $dump_file) {
	echo "restoring database from dumpfile\n";
	exec("mysql -u" . Config::get('database.username') . " -p" . Config::get('database.password') . " " . Config::get('database.database') . " < " . $dump_file);

}

function RunMigrations($w) {
	echo "set migration mode\n";
	$w->db->setMigrationMode(true);
	$w->Migration->installInitialMigration();
    $w->Migration->runMigrations("all");
}

//wipe the database
echo "cleaning db\n";
DropTablesDatabase($w);
RunMigrations($w);
echo "database emptied\n";


//function for running suites
function runSuite($w, $suite, $path, $url, $results) {
	cleanDatabase($w);
	//check for database seed file
	$seed_file = $path . "/dbseed.sql";
	if (file_exists($seed_file)) {
		echo "seed file found for module " . $path . "\n";
		$w->db->clear_sql();
		$w->db->exec(file_get_contents($seed_file));
	} else {
		echo "no seed file found for module: " . $path . "\n";
	}
	$results_filename = "results" . basename($suite);
	exec("touch ../" . $results_filename);
	//exec('java -jar /usr/bin/selenium-server-standalone-2.53.1.jar -htmlSuite "*firefox" "' . $url . '" "' . $suite . '" "../' . $results_filename . '"', $test_result, $return_var);
	exec('java -jar /usr/bin/selenium-server-standalone-2.53.1.jar -htmlSuite "*firefox" "' . $url . '" "' . $suite . '" "../' . $results_filename . '" 2>&1', $test_result, $return_var);
	//echo "--------------------------------------------------------------------------\n";
	//print_r($return_var); echo "\n";
	$results[] = [basename($suite),$test_result];
	return $results;
}

//function for running tests from test folder
function runModuleTests($w,$path, $url, $results, $test_name) {

	//find suite files
	$suites = glob($path ."/*suite*.html");
	if (!empty($suites)) {
		foreach ($suites as $suite) {
			if (stripos($suite,$test_name)) {
				$results = runSuite($w,$suite, $path, $url, $results);
			}
		}
	}
	return $results;

}

//function for finding test folders recursively
function findSeleniumFolders($w, $path, $paths) {
	//echo "checking path: " . $path . "\n";
	foreach (scandir($path) as $file) {
		//echo "checking file: " . $file . "\n";
		if ($file == "selenium") {
			//echo "found one: " . $path . "/selenium\n";
			$paths[] = $path . '/selenium';
			continue;
		}
		if ($file != '.' && $file != '..') {
			$newPath = $path . "/" . $file;
			if (is_dir($newPath)) {
				//echo "recursion occuring\n";
				$paths = findSeleniumFolders($w, $newPath, $paths);
			}

		}
	}
	return $paths;
}

//find and initialise cmfive tests first

if (is_dir($root_path . '/system/tests/selenium') && file_exists($root_path . '/system/tests/selenium/Initial_test.html')) {
	echo "running initial test\n";
	runSuite($w, $root_path . '/system/tests/selenium/Initial_test.html', $root_path . '/system/tests/selenium');
} else {
	echo "initial test not found\n";
	echo is_dir($root_path . '/system/tests/selenium'); echo "\n";
	echo file_exists($root_path . '/system/tests/selenium/Initial_test.html'); echo "\n";
	echo "path = " . $root_path . "/system/tests/selenium/Initial_test.html\n";
}

//find all other test folders and run tests
echo "finding test folders\n";
$module_test_paths = findSeleniumFolders($w, $root_path, []);
//print_r($module_test_paths); echo "\n";

echo "Running Tests\n";
//run tests for each test folder
foreach ($module_test_paths as $path) {
	$results = runModuleTests($w,$path, $url, $results, $test_name);
}


function checkResults($results,$results_summary) {
	$failed_string = "status_failed";

	if (!empty($results)) {
		foreach($results as $key => $result){
			$last_result_line = end($result[1]);
			if (stripos($last_result_line,"Killing") || stripos($last_result_line,"Shutting")) {
				$results_summary[] = [$result[0], "Tests Passed"];
			} else {
				$results_summary[] = [$result[0], $last_result_line];
			}
		}
	}
	return $results_summary;
}

$results_summary = checkResults($results,[]);

DropTablesDatabase($w);
RestoreDatabase($w, $dump_file);
//print_r($results);
print_r($results_summary); echo "\n";
