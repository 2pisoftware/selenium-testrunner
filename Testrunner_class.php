<?php

/**
 * Static class wrapper for testrunner functionality
 *
 * @author Adam Buckley <adam@2pisoftware.com>
 */
class Testrunner {

	const INFO = 'info';
	const ERROR = 'error';
	const SUCCESS = 'success';
	const DEBUG = 'debug';


	/**
	 * Backs up database to $file
	 *
	 * @param String $file
	 */
	public static function backupDatabase($file) {
		self::output("Backing up database to $file");
        $host = !empty(Config::get('database.hostname')) ? Config::get('database.hostname') : 'localhost';
        $password = Config::get('database.password');
        if (empty($password)) {
        	$password = '';
        } else {
        	$password = ' -p' . $password;
        }
		exec("mysqldump -h" . $host . " -u" . Config::get('database.username') . $password . " " . Config::get('database.database') . " > " . $file);
	}

	/**
	 * Empties the tables in the database. Will loop through all listed
	 * tables and empty one by one (was called cleanDatabase)
	 *
	 * @param Web $w
	 * @throws Exception
	 */
	public static function emptyTables(Web $w) {
        self::output("attempting to empty tables");
		$db_name = Config::get('database.database');
		$tb_results = $w->db->query("show full tables where Table_Type != 'VIEW'");
		if(isset($tb_results)) {
	        if($tb_results->rowCount() > 0) {

	        	// Loop through all tables and drop them
	        	foreach($tb_results as $row) {
	            	$w->db->clear_sql();
	                $w->db->exec("TRUNCATE TABLE {$row[0]};");

	                $w->db->clear_sql();
	                $t_count = $w->db->query("SELECT COUNT(*) FROM {$row[0]};")->fetchAll();

	                if ($t_count[0][0] > 0) {
	                	self::output("Truncate failed for table: " . $row[0] . ". Currently has " . $t_count[0][0]);
	                }
	            }
	        }
	    } else {
	        throw new Exception("Could not retrieve tables for \"$db_name\"");
	    }
	}

	/**
	 * Drops the tables in the database. Will loop through all listed
	 * tables and drop one by one (was called DropTablesDatabase)
	 *
	 * @param Web $w
	 * @throws Exception
	 */
	public static function dropTables(Web $w) {
		self::output("Dropping tables");

		$db_name = Config::get('database.database');
		$tb_results = $w->db->query("show full tables where Table_Type != 'VIEW'");
	    if(isset($tb_results)) {
	        if($tb_results->rowCount() > 0) {

	        	// Drop all tables
	        	foreach($tb_results as $row) {
	        		// self::output("Tables or View");
	        		// self::output(print_r($row));
	            	$w->db->clear_sql();
	                $w->db->exec("DROP TABLE {$row[0]};");
	            }

	            // Verify that all tables are gone
	            $tb_results_second_opinion = $w->db->query("show full tables where Table_Type != 'VIEW'");
	            if(isset($tb_results_second_opinion)) {
	                if($tb_results_second_opinion->rowCount() > 0) {
	                    throw new Exception("Database \"$db_name\" could not be emptied. Currently has " . $tb_results_second_opinion->rowCount() . " rows");
	                }
	            } else {
	                throw new Exception("Could not perform sql recount of tables for \"$db_name\".");
	            }
	        }

	        self::output("Tables dropped");
	    } else {
	        throw new Exception("Could not retrieve tables for \"$db_name\"");
	    }
	}

	/**
	 * Launches selenium and runs given suite file
	 *
	 * @param Web $w
	 * @param String $suite test suite to run
	 * @param String $path
	 * @param String $url
	 * @param Array $results
	 * @param String $graphical
	 * @return Array $results
	 */
	public static function runSuite($w, $suite, $path, $testrunner_path, $url, Array &$results = [], $graphical = 'false',$user_defined_suite = 'all') {
		$current_path = exec('pwd');
		
        
        if ($user_defined_suite == 'all' || '/' . $user_defined_suite . '.html' == str_replace($path, "", $suite) ) {
            
        } else {
            return;
        }
		self::emptyTables($w);
        self::clearCache($w);
        self::runSeeds($w); 

		self::output("Running test suite: $suite");
        self::output("TESTING: " . str_replace($path, "", $suite));
        
        

		// Check for database seed file
		$seed_file = $path . "/dbseed.sql";
		if (file_exists($seed_file)) {
			self::output("Seed file found for module " . $path);
			$w->db->clear_sql();
			$w->db->exec(file_get_contents($seed_file));
            self::output("Seed file ran");
            self::output("TESTING LOCATION " . ROOT_PATH . " " . Config::get('database.database'));
		} else {
			self::output("No seed file found for module: " . $path);
		}

		$nice_url = str_replace(["http://", ".", '/'], ["", "_"], $url);
		$results_folder = $testrunner_path . "/results/" . $nice_url;
		if (!file_exists($results_folder)) {
			exec("mkdir " . $results_folder);
		}
		$results_filename = $results_folder . "/" . $nice_url . "_" . basename($suite);
		exec("touch " . $results_filename);
		self::output("Launching selenium");
		exec(($graphical == 'false' ? 'xvfb-run -a ' : '') . 'java -jar ' . $testrunner_path . '/selenium-server-standalone-2.53.1.jar -firefoxProfileTemplate ' .$testrunner_path . '/firefox-profile/ -htmlSuite "*firefox ' . $testrunner_path . '/firefox/firefox" "' . $url . '" "' . $suite . '" "' . $results_filename . '" 2>&1', $test_result, $return_var);

		$results[] = [basename($suite), $test_result];
		return $results;
	}

	/**
	 * Finds suite files and runs them
	 *
	 * @param Web $w
	 * @param String $suite test suite to run
	 * @param String $path
	 * @param String $url
	 * @param Array $results
	 * @param String $graphical
	 * @return Array $results
	 */
	public static function runModuleTests(Web $w, $path, $testrunner_path, $url, Array &$results = [], $graphical = 'false', $user_defined_suite = 'all') {
		// Find suite files
		$suites = glob($path . "/" . "*suite*.html");

		if (!empty($suites)) {
			self::output("Running suites");
			foreach ($suites as $suite) {
				self::runSuite($w, $suite, $path, $testrunner_path, $url, $results, $graphical, $user_defined_suite);
			}
		}

		return $results;
	}

	/**
	 * Recursively looks for selenium folders
	 *
	 * @param Web $w
	 * @param String $path
	 * @param Array $paths
	 */
	public static function findSeleniumFolders(Web $w, $path, Array &$paths = []) {
        //keep out of composer directory
        
        if (substr($path, strlen($path) - 8) == "composer") {
            return $paths;
        }
        
		foreach (scandir($path) as $file) {
			if ($file == "selenium") {
				self::output("found tests at: " . $path . '/selenium');
				$paths[] = $path . '/selenium';
				continue;
			}

			if ($file != '.' && $file != '..') {
				$newPath = $path . "/" . $file;
				if (is_dir($newPath)) {
					$paths = self::findSeleniumFolders($w, $newPath, $paths);
				}

			}
		}

		return $paths;
	}

	/**
	 * Runs all migrations in Cmfive
	 *
	 * @param Web $w
	 */
	public static function runMigrations(Web $w) {
		self::output("Running migrations");
		$w->db->setMigrationMode(true);
		$w->Migration->installInitialMigration();
	    $w->Migration->runMigrations("all");
	    self::output("Migrations run");
	    self::runSeeds($w);
	}

	/**
	 * Runs all migrations seeds
	 */
	public static function runSeeds(Web $w) {
		// run all seeds
	    self::output("finding seed files");
	    $seeds = $w->Migration->getSeedMigrations();
	    foreach($seeds as $module => $available_seeds) {
	    	foreach($available_seeds as $path => $classname) {
	    		if (!$w->Migration->migrationSeedExists($classname)) {
	    			self::output("running seed: " . $classname);
	    			require_once($path);
					$runSeed = $classname;
					$seed_obj = new $classname($w);
					$seed_obj->seed();

					$migration_seed = new MigrationSeed($w);
					$migration_seed->name = $classname;
					$migration_seed->insert();
					self::output("seed run");
	    		}
	    	}
	    }
	}

	/**
	 * Restores the database
	 *
	 * @param Web $w
	 * @param string $dump_file dump file path
	 * @throws Exception
	 */
	public static function restoreDatabase(Web $w, $dump_file) {
		self::output("Restoring database from dumpfile");

		if (!file_exists($dump_file)) {
			throw new Exception("Cannot find or open file: " . $dump_file);
		}
        $host = !empty(Config::get('database.hostname')) ? Config::get('database.hostname') : 'localhost';
        $password = Config::get('database.password');
        if (empty($password)) {
        	$password = '';
        } else {
        	$password = ' -p' . $password;
        }
		exec("mysql -h " . $host . " -u" . Config::get('database.username') . $password . " " . Config::get('database.database') . " < " . $dump_file);
	}

	/**
	 * Organises results from tests
	 *
	 * @param Array $results
	 * @return Array organised results
	 */
	public static function checkResults($results) {
		$results_summary = [];

		if (!empty($results)) {
			foreach($results as $key => $result){
				$last_result_line = end($result[1]);
				if (stripos($last_result_line,"Killing") || stripos($last_result_line,"Shutting")) {
					$results_summary[] = [$result[0], "Tests Passed"];
				} else {
					$results_summary[] = [$result[0], $last_result_line];
				}
			}
		}
		return $results_summary;
	}

	/**
	 * Outputs to screen
	 *
	 * @param String $text
	 */
	public static function output($text, $type = null, $newLine = true) {
		if (empty($type)) {
			$type = self::INFO;
		}

		static $RED = "\033[0;31m";
		static $GREEN = "\033[0;32m";
		static $BLUE = "\033[0;34m";
		static $YELLOW = "\033[1;33m";
		static $NC = "\033[0m";

		switch($type) {
			case self::INFO:
				echo $NC;
				break;
			case self::DEBUG:
				echo $BLUE;
				break;
			case self::SUCCESS:
				echo $GREEN;
				break;
			case self::ERROR:
				echo $RED;
				break;
			default:
				echo $NC;
		}
		if ($newLine) {
			echo $text . $NC . PHP_EOL;
		} else {
			echo $text . $NC;
		}
	}

    public static function clearCache($w) {
        self::output("Clearing cache");
        if(is_file(ROOT_PATH.'/cache/classdirectory.cache')) {
			unlink(ROOT_PATH.'/cache/classdirectory.cache');
		}
		if(is_file(ROOT_PATH.'/cache/config.cache')) {
			unlink(ROOT_PATH.'/cache/config.cache');
		}
        self::output("Clearing uploads");
        array_map('unlink', glob(ROOT_PATH."/uploads"));
    }

	public static function prompt($message, $conditions = null) {
		// Conditions is an array of posibilities with keys as possible answers and values as returns
		self::output($message, 'success', false);
		while (true) {
			$choice = trim(fgets(STDIN));
			if ($choice)  {
                if ($conditions) {
    				foreach ($conditions as $key => $value) {
    				    if ($choice == $key) {
                            return $value;
                        }
    				}
                } else {
                    return $choice;
                }
			} else {
				self::output($message, 'success', false);
			}
		}
	}
    
    public static function promptTestSuite($w) {
        return self::prompt("run all test suites? (type 'all' or test suite name)");
    }

	public static function promptEmail(Web $w) {
		// Waits for input on from the user
		 $ask = self::prompt('Do you want the results emailed? [Y/N] ', ['Y'=>true, 'y'=>true, 'n'=>false, 'N'=>false]);

		if ($ask) {
			$username = Config::get('email.username');
			if (empty($username)) {
				self::output("No email username found, specify it in your cmfive-boilerplate/config.php", 'error');
				return false;
			}
			$password = Config::get('email.password');
			if (empty($password)) {
				self::output("No email password found, specify it in your cmfive-boilerplate/config.php", 'error');
				return false;
			}
			$to = self::prompt('Enter an email to send results to: ');
            return $to;
		} else {
			self::output("Ok then", 'success');
			return false;
		}
	}

	public static function sendResults(Web $w, $to, $results_path) {
		$attachments = [];
		$dir = dir($results_path . "/results");
		while (false !== ($folder = $dir->read())) {
			if ($folder != '..' and $folder != '.') {
				$folder_path = dir($results_path . "/results/" . $folder);
				self::output("Found results folder " . $folder);
				while (false !== ($entry = $folder_path->read())) {
				   if (pathinfo($entry)['extension'] == 'html') {
				       self::output("Found result file: " . $entry);
				       $attachments[] = $results_path . "/results/" . $folder . '/'. $entry;
				   }
			   	}
			}

	   	}

		if (!empty($attachments)) {
            $replyto = Config::get('email.username');
			// Add url to subject
			$subject = "Cmfive Selenium test results " . date('Y-m-d H:i:s');
			$body = 'Test Results are attached';
			$w->Mail->sendMail($to, $replyto, $subject, $body, null, null, $attachments);
			self::output("Email sent", 'success');
		} else {
			self::output("No result files found");
		}
	}
}
