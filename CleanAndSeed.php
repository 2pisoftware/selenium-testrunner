<?php
/**
* Testrunner
* @author isaac lynnah, isaac@2pisoftware.com, February 2017
*
* set just seed and clean the database, useful for development.
*/
require_once("Testrunner_class.php");
//receive the root directory from command parameters
$root_path = $argv[1];
$module = !empty($argv[2]) ? $argv[2] : null;


//instantiate cmfive Web for configuration access

//move to the project directory
//-------------------------------------------------
chdir($root_path);
// require Web. needed for accessing database credentials
require "system/web.php";
//instantiate Web
$w = new Web();
session_start();
//user Web functions to create database connection
$w->initDB();
//create function for emptying the database

Testrunner::output('CAUTION, will drop the database table associated with the site, no automatic restoration is provided', Testrunner::INFO);
$dump_file = "db_dump.sql";
Testrunner::backupDatabase($dump_file);
if (file_exists($dump_file)) {
    Testrunner::output("DB backup successful", Testrunner::SUCCESS);
} else {
    // Do not continue if we cannot snapshot the database
    Testrunner::output("DB backup failed", Testrunner::ERROR);
    die();
}
Testrunner::dropTables($w);
Testrunner::runMigrations($w);
if (!empty($module)) {
    $seed_file = $root_path . "modules/" . $module . "/tests/selenium/dbseed.sql";
} else {
    $seed_file = $root_path . "/system/tests/selenium/dbseed.sql";
}
echo $seed_file;
if (file_exists($seed_file)) {
    Testrunner::output("Seed file found for module ");
    $w->db->clear_sql();
    $w->db->exec(file_get_contents($seed_file));
    Testrunner::output("Seed file ran");
} else {
    Testrunner::output("No seed file found for module: ");
}
