<?php
//a collection of helper functions for storing and restoring the database to specific points
//the aim is to be able to store database snapshots, give them a name and retore them on demand


require_once("Testrunner_class.php");

session_start();

$root_path = $argv[1];
//name for backup
$name = $argv[2];
//action name: 'save', 'restore', 'clean'
$action = $argv[3];

chdir($root_path);

// Require and create Web. Needed for accessing database credentials
require "system/web.php";
$w = new Web();

// Ensure any new log files have the correct permissions
$w->log->setLogger("TESTING")->info("This is the Test Runner");
exec("chmod -R oug+wrx " . $root_path . "/log");

// Create a database connection
$w->initDB();

if ($action == 'save') {
    // Backup database to restore after testing
    $dump_file = $name . ".sql";
    Testrunner::backupDatabase($dump_file);

    if (file_exists($dump_file)) {
    	Testrunner::output("DB backup successful", Testrunner::SUCCESS);
    } else {
    	// Do not continue if we cannot snapshot the database
    	Testrunner::output("DB backup failed", Testrunner::ERROR);
    	die();
    }
}

if ($action == 'restore') {
    Testrunner::dropTables($w);
    Testrunner::restoreDatabase($w, $name . ".sql");
}
