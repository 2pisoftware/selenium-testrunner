
<?php

//receive the root directory from command parameters
$root_path = $argv[1];
$module = $argv[2];

chdir($root_path);
// require Web. needed for accessing database credentials
require "system/web.php";
//instanciate Web
$w = new Web();

//user Web functions to create database connection
$w->initDB();

//function for running suites
function runSuite($w, $suite, $path) {
	//cleanDatabase($w);
	//check for database seed file
	$seed_file = $path . "/dbseed.sql";
	if (file_exists($seed_file)) {
		echo "seed file found for module " . $path . "\n";
		$w->db->clear_sql();
		$w->db->exec(file_get_contents($seed_file));
	} else {
		echo "no seed file found for module: " . $path . "\n";
	}

}

if (!empty($module)) {
	if (is_dir($root_path . '/modules/' . $module . '/tests/selenium')){
		echo "module path found\n";
		runSuite($w, $root_path . '/modules/' . $module . '/tests/selenium/Initial_test.html', $root_path . '/modules/' . $module . '/tests/selenium');
	} else {
		echo "could not find module\n";
	}
} else {
	if (is_dir($root_path . '/system/tests/selenium')){
		runSuite($w, $root_path . '/system/tests/selenium/Initial_test.html', $root_path . '/system/tests/selenium');
	}
}
