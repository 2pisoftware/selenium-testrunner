<?php

/**
 * Testrunner
 * @author isaac lynnah, isaac@2pisoftware.com, February 2017
 * @author Jack Robbers, jack@2pisoftware.com, March 2017
 * @author Adam Buckley <adam@2pisoftware.com>
 *
 * To run tests place this file above your cmfive root directory
 * and on the command line type 'sudo php testrunner.php your/cmfive/root/path "http://your.url"'
 */

require_once("Testrunner_class.php");

session_start();

$root_path = $argv[1]; // The root folder of your cmfive installation
$url = $argv[2]; // The url of your cmfive installation
$graphical = $argc > 3 ? $argv[3] : 'false'; //set this flag if you want to run tests with a graphical interface
$to = $argc > 4 ? $argv[4] : ''; 
$current_path = exec('pwd');

// First check that a dependant software is installed
// Check that java is installed
$java_path = exec('which java');
if (empty($java_path)) {
	Testrunner::output("Java is required to run selenium tests, please install Java then try again", Testrunner::ERROR);
} else {
	Testrunner::output('Java found at ' . $java_path, Testrunner::SUCCESS);
}
// Testrunner::output(exec("apt-get install default-jdk -y"));

// Check that the correct version of firefox is installed. Selenium tests require firefox 47
$install_firefox = true;

// Checks if Firefox installed locally and the version is correct
if (file_exists($current_path . '/firefox/')) {
	exec("$current_path/firefox/firefox -version", $firefox_version);

	if($firefox_version[0] == "Mozilla Firefox 47.0.1") {
		Testrunner::output($firefox_version[0] . " found", Testrunner::SUCCESS);
		$install_firefox = false;
	}
}

if ($install_firefox) {
	Testrunner::output("Wrong firefox version or firefox not installed", Testrunner::ERROR);

	// Downloads the Firefox tar if it doesn't exist
	if (!file_exists("firefox-47.0.1.tar.bz2")) {
		Testrunner::output("Downloading firefox executable", Testrunner::DEBUG);
		exec("wget https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/47.0.1/linux-x86_64/en-US/firefox-47.0.1.tar.bz2");
	}

	// Uncompresses archive to local directory
	Testrunner::output("Unzipping firefox to current directory", Testrunner::DEBUG);
	exec("tar -xjvf firefox-47.0.1.tar.bz2");

	// Tries to execute it and check the version
	exec("$current_path/firefox/firefox -version", $firefox_version);
	if($firefox_version[0] == "Mozilla Firefox 47.0.1") {
		Testrunner::output($firefox_version[0] . " is installed", Testrunner::SUCCESS);
	} else {
		Testrunner::output("Unable to install firefox properly - please investigate", Testrunner::ERROR);
		exit();
	}
}


// Check that selenium server 2.53.1 is installed
if (file_exists("selenium-server-standalone-2.53.1.jar")) {
	Testrunner::output("Selenium v2.53.1 found", Testrunner::SUCCESS);
} else  {
	Testrunner::output("Selenium v2.53.1 not found, downloading", Testrunner::ERROR);

	// Download selenium to /usr/bin directory
	exec("wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar -P ./");

	if (file_exists("selenium-server-standalone-2.53.1.jar")) {
		Testrunner::output("Selenium downloaded");
	} else {
		Testrunner::output("Selenium failed to download - please investigate", Testrunner::ERROR);
		exit();
	}
}

// Check that xvfb is installed
$xvfb_path = exec('which xvfb-run');
if (empty($xvfb_path) && $graphical !== 'true') {
	Testrunner::output("xvfb is required to run selenium graphically", Testrunner::ERROR); die;
	// Testrunner::output(exec("apt-get install xvfb"));
	// Testrunner::output("xvfb installed");
} else {
	Testrunner::output('xvfb found at ' . $xvfb_path, Testrunner::SUCCESS);
}

// Check that a firefox profile has been created
if(!file_exists($current_path .'/firefox-profile')) {
	exec("xvfb-run -a $current_path/firefox/firefox -CreateProfile 'test_runner $current_path/firefox-profile'");
	Testrunner::output('firefox profile created', Testrunner::SUCCESS);
}

if(!file_exists($current_path . '/results/')) {
	exec("mkdir results");
	Testrunner::output('Made a results folder', Testrunner::SUCCESS);
}
// clear previous results
$nice_url = str_replace(["http://", ".", '/'], ["", "_"], $url);
exec('rm -rf results/' . $nice_url . '/*');

Testrunner::output('');

// All dependancies now accounted for.

// Move to the project directory
chdir($root_path);

// Require and create Web. Needed for accessing database credentials
require "system/web.php";
$w = new Web();

// Ensure any new log files have the correct permissions
$w->log->setLogger("TESTING")->info("This is the Test Runner");
exec("chmod -R oug+wrx " . $root_path . "/log");

// $to is read from the command line first,
// then the enironment variable TESTRUNNER_RESULT_ADDRESS, make sure they are accessible from all shells. I couldn't get it show up when running this script with sudo. We shouldn't be doing that anyway.
// Lastly prompt for the address.
if (empty($to)) {
	if (getEnv('TESTRUNNER_RESULT_ADDRESS')) {
		$to = getEnv('TESTRUNNER_RESULT_ADDRESS');
	} else {
		$to = Testrunner::promptEmail($w);
	}
}

if (!empty($to)) {
	Testrunner::output('Emailing results to ' . $to . ' on completion of tests', 'success');
	Testrunner::output('');
}

$user_defined_suite = Testrunner::promptTestSuite($w);
// $user_defined_suite = 'all';

// Create a database connection
$w->initDB();

// Backup database to restore after testing
$dump_file = "db_dump.sql";
Testrunner::backupDatabase($dump_file);

if (file_exists($dump_file)) {
	Testrunner::output("DB backup successful", Testrunner::SUCCESS);
} else {
	// Do not continue if we cannot snapshot the database
	Testrunner::output("DB backup failed", Testrunner::ERROR);
	die();
}

try {
	// Wipe the database for testing
	Testrunner::dropTables($w);
	Testrunner::runMigrations($w);

	$results = [];

	Testrunner::output('');

	// Run system tests
	if (is_dir($root_path . '/system/tests/selenium') && file_exists($root_path . '/system/tests/selenium/Initial_test.html')) {
		Testrunner::runSuite($w, $root_path . '/system/tests/selenium/Initial_test.html', $root_path . '/system/tests/selenium', $current_path, $results, $graphical, $user_defined_suite);
	}

	// Find all other test folders and run tests
	Testrunner::output("Finding test folders for path: " . $root_path);
	$module_test_paths = Testrunner::findSeleniumFolders($w, $root_path);

	// Run tests for each test folder
	foreach ($module_test_paths as $path) {
		Testrunner::runModuleTests($w, $path, $current_path, $url, $results, $graphical, $user_defined_suite);
	}

	$results_summary = Testrunner::checkResults($results);

	Testrunner::output("\nResults\n---------------------------------------------------");
	for($i = 0; $i < count($results_summary); $i++) {
		Testrunner::output("$i\t" . $results_summary[$i][0] . "\t" . $results_summary[$i][1]);
	}
	Testrunner::output('');
} catch (Exception $e) {
	Testrunner::output("Exception found: " . $e->getMessage(), Testrunner::ERROR);
	Testrunner::output("Trace:\n" . $e->getTraceAsString());
} finally {
	Testrunner::dropTables($w);
	Testrunner::restoreDatabase($w, $dump_file);
	if (!empty($to)) {
		Testrunner::sendResults($w, $to, $current_path);
	}
}
